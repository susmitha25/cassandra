$(document).ready(function () {
	$('#submit').click(function () {
		if ($('#username').val().trim() !== "" && $('#password').val().trim() !== "") {
			$.post( "/api/login", {
				username: $('#username').val(),
				password: $('#password').val()
			}).done(function(data) {
				if (data === "true") {
					window.location.href = '/logedin';
				} else {
					$('#status').text(data);
				}
			});
		} else {
			$('#status').text("Please fill all the fields.");
		}
	});
});
