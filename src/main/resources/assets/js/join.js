$(document).ready(function () {
	$('#submit').click(function () {
		if ($('#username').val().trim() !== "" && $('#password').val().trim() !== "" && $('#confirmPass').val().trim() !== "") {
			$.post( "/api/signup", {
				username: $('#username').val(),
				password: $('#password').val(),
				confirmPass: $('#confirmPass').val()
			}).done(function(data) {
				if (data === "true") {
					window.location.href = '/logedin';
				} else {
					$('#status').text(data);
				}
			});
		} else {
			$('#status').text("Please fill all the fields.");
		}
	});
});