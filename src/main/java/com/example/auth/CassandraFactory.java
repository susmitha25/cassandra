package com.example.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by susmitha on 5/5/15.
 */
public class CassandraFactory {


    private String keyspace;

    private String host;


    public String getKeyspace(){
        return keyspace;
    }
    public String getHost(){
        return host;
    }
    public void setKeyspace(String keyspace){
        this.keyspace=keyspace;
    }
    public void setHost(String host){
        this.host=host;
    }

}
