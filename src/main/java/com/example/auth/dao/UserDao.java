package com.example.auth.dao;


import com.example.auth.core.User;


import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;
/**
 * Created by susmitha on 23/4/15.
 */

@Accessor
public interface UserDao {

    @Query("insert into person (username,password) values (?,?)")
   ResultSet insert( String username,String password);



   @Query("select * from person where username =? ")
   User findPass(String username);



}
