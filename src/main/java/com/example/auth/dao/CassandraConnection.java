package com.example.auth.dao;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;


/**
 * Created by susmitha on 5/5/15.
 */
public class CassandraConnection {

    private Cluster cluster;

    public Cluster getCluster() {
        return cluster;
    }

    private Session session;

    public Session getSession() {
        return session;
    }


    public CassandraConnection(String host, String keyspace) {
        Cluster cluster = Cluster.builder().addContactPoint(host).build();
        session = cluster.connect(keyspace);

    }
}
