/**
 * Created by susmitha on 16/4/15.
 */
package com.example.auth;

import com.example.auth.dao.UserDao;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import com.example.auth.resources.LoginResource;
import com.example.auth.dao.CassandraConnection;

import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.views.ViewBundle;
import org.eclipse.jetty.server.session.SessionHandler;
import org.skife.jdbi.v2.DBI;
import io.dropwizard.assets.AssetsBundle;
import com.datastax.driver.mapping.MappingManager;


import com.bazaarvoice.dropwizard.assets.ConfiguredAssetsBundle;




public class LoginApplication extends Application<LoginConfiguration>{
    public static void main(String[] args) throws Exception {
        new LoginApplication().run(args);

    }



    public void initialize(Bootstrap<LoginConfiguration> bootstrap) {
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );
        bootstrap.addBundle(new AssetsBundle("/assets", "/public", "favicon.ico"));
        bootstrap.addBundle(new AssetsBundle("/favicon.ico", "/assets/favicon.ico", null, "favicon"));
        bootstrap.addBundle(new ViewBundle());



    }

    @Override
    public void run(LoginConfiguration configuration,
                    Environment environment) {
        MappingManager manager = new MappingManager (new CassandraConnection(configuration.getCassandraFactory().getHost(),configuration.getCassandraFactory().getKeyspace()).getSession());
        UserDao dao = manager.createAccessor(UserDao.class);

        final LoginResource resource = new LoginResource(dao);
        environment.jersey().register(resource);
        environment.servlets().setSessionHandler(new SessionHandler());
    }

}
