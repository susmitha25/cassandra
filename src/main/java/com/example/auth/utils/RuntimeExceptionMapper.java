package com.example.auth.utils;

/**
 * Created by susmitha on 28/4/15.
 *
 */
import com.example.auth.view.ErrorView;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

public class RuntimeExceptionMapper implements ExceptionMapper<RuntimeException> {
    public Response toResponse(RuntimeException exception) {
        Response response = Response
                .serverError()
                .entity(new ErrorView(exception))
                .build();
        return response;
    }
}
