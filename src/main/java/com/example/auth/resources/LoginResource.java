package com.example.auth.resources;



import com.datastax.driver.core.ResultSet;
import com.example.auth.core.User;
import com.example.auth.dao.UserDao;
import com.example.auth.utils.HashingUtil;
import com.example.auth.view.JoinView;
import com.example.auth.view.LoginView;
import com.example.auth.view.UserView;
import io.dropwizard.jersey.sessions.Session;
import io.dropwizard.views.View;
import com.example.auth.utils.RoutingUtil;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.lang.String;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Path("/")
@Produces(MediaType.TEXT_HTML)

public class LoginResource {

    private UserDao userDao;
    User user;


    public LoginResource(UserDao dao1){
        super();
        this.userDao = dao1;


    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public UserView getPerson(@Session HttpSession session) {
        RoutingUtil.authenticate(session);
        User a=new User();
        a.setUsername(session.getAttribute("username").toString());
        return new UserView(a);
    }
    @Path("api/")
    @POST
    @Produces(MediaType.TEXT_HTML)
    public Boolean logout(@Session HttpSession session) {
        session.setAttribute("username", "");
        session.invalidate();

        return true;
    }

    @Path("logedin")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public UserView getLogedin(@Session HttpSession session) {
        RoutingUtil.authenticate(session);
        User a=new User();
        a.setUsername(session.getAttribute("username").toString());
        return new UserView((a));
    }
    @Path("api/logedin")
    @POST
    @Produces(MediaType.TEXT_HTML)
    public String setLogedin(@Session HttpSession session) {
        session.setAttribute("username", "");
        session.invalidate();

        System.out.println("\n\nhhhh");
        return "true";

    }
    @Path("login")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public LoginView getLogin() {
        return new LoginView("");
    }

    @Path("api/login")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String postLogin(@FormParam("username") String username, @FormParam("password") String password,@Session HttpSession session) {


        int id = 0;
        user = new User();
        user.setPassword(password);
        user.setUsername(username);

        User userpass = userDao.findPass(username);

        String pass=userpass.getPassword();



        if (!HashingUtil.GenerateHash(password).equals(pass)) {
            user = new User();
            user.setUsername(username);
            user.setPassword("");

            return "incorrect password";
        }
        else {
            session.setAttribute("username", username);

            return "true";
        }


    }


    @Path("signup")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public JoinView getJoin() {
        return new JoinView("");
    }

    @Path("api/signup")
    @POST
    @Produces(MediaType.TEXT_HTML)
    public String postJoin(@FormParam("username") String username, @FormParam("password")String password, @FormParam("confirmPass") String confirmPass,@Session HttpSession session) {

        user=new User();
        user.setPassword(password);
        user.setUsername(username);

        User userpass=userDao.findPass(username);
        if(userpass!=null)
        {
            return "such a user already exists";
        }

        Pattern p = Pattern.compile("[^a-z0-9_ ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(username);
        boolean b = m.find();
        if(username.trim().equals("") || password.trim().equals("") || confirmPass.trim().equals("")){
            return "enter all feilds!!";
        }
        else if(username.length()>30)
        {
            return "username exceeds limit";
        }
        else if(password.length()<5){
            return "password shoud have atleast 5 characters";
        }
        else  if(password.length()>30){
            return "password exceeds limit";
        }
        else if(username.contains(" ")||b){
            return "username cannot contain blanckspaces or special characters";
        }


        else if (!password.equals(confirmPass)) {
            return "passwords dont match";
        }
        else {
            userDao.insert(username, HashingUtil.GenerateHash(password));
            session.setAttribute("username", username);
            return "true";
        }

    }
}


