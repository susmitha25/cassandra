package com.example.auth.view;
import io.dropwizard.views.View;

/**
 * Created by susmitha on 27/4/15.
 */
public class JoinView extends View{

    String msg;

    public JoinView() {
        super("join.mustache");
    }
    public JoinView(String msg) {
        super("join.mustache");

        this.msg=msg;
    }

    public String getMsg() {
        return msg;
    }


}

