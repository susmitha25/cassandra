package com.example.auth.view;

import io.dropwizard.views.View;

public class ErrorView extends View {
    private String msg;

    public ErrorView(RuntimeException msg) {
        super("error.mustache");
        this.msg = msg.getMessage();
    }

    public String getMsg() {
        return msg;
    }
}
