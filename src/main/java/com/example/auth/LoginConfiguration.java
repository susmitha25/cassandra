/**
 * Created by susmitha on 16/4/15.
 */
package com.example.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import com.bazaarvoice.dropwizard.assets.AssetsBundleConfiguration;
import com.bazaarvoice.dropwizard.assets.AssetsConfiguration;
import io.dropwizard.Configuration;


public class LoginConfiguration extends Configuration implements AssetsBundleConfiguration{


    @JsonProperty("cassandraDb")
    private CassandraFactory cassandraDb;


    @JsonProperty("cassandraDb")
    public CassandraFactory getCassandraFactory() {
        return cassandraDb;
    }


    @JsonProperty
    private final AssetsConfiguration assets = new AssetsConfiguration();

    public AssetsConfiguration getAssetsConfiguration() {
        return assets;
    }
}
